/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {

    private final char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private final Player player1;
    private final Player player2;
    private Player currentPlayer;
    private int turnCount = 0;
    private int row;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlayer.getSymbol();
            this.row = row;
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        turnCount++;
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    public boolean checkWin() {
        return checkRow();
    }

    public boolean checkRow() {
        return table[row-1][0] != '-' && table[row-1][0] == table[row-1][1] && table[row-1][2] == table[row-1][1];
    }

    public boolean checkDraw() {
        return turnCount == 9;
    }

}
